package com.example.davidbernal.todo_list


import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.text.InputType
import android.view.Menu
import android.view.MenuItem
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.database.*
import kotlinx.android.synthetic.main.activity_ma.*
import java.util.*

import com.google.firebase.database.ChildEventListener
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import jonathan.moviles.epn.todo_list.ListSelectionRecyclerViewAdapter.ListSelectionRecyclerViewClickListener

class ListDetailActivity : AppCompatActivity() {

    lateinit var listRecyclerView: RecyclerView;
    val dataBase = FirebaseDatabase.getInstance();
    val ref = dataBase.getReference("todo-list");

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_detalle)

        val listId = intent.getStringExtra(Activitylist.INTENT_LIST_ID);
        val ref2 = dataBase.getReference("todo-list/"+listId+"/list");
        ref.child(listId).child("list-name")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onCancelled(p0: DatabaseError) {

                }

                override fun onDataChange(dataSnapshot: DataSnapshot) {
                    title = dataSnapshot.value.toString();
                }

            })



        listRecyclerView = findViewById(R.id.list_recycler_view);
        listRecyclerView.layoutManager = LinearLayoutManager(this);
        listRecyclerView.adapter = ListSelectionRecyclerViewAdapterListDeList(this, ref2);


        fab.setOnClickListener { view ->
            //Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
            //   .setAction("Action", null).show()
            showCreateListDialog(listId);
        }

    }

    private fun showCreateListDialog(listId:String){
        val dialogTitle = getString(R.string.neme_of_list);
        val positiveButtonTitle = getString(R.string.create_list);

        val builder = AlertDialog.Builder(this);
        val listTitleEditText = EditText(this);
        listTitleEditText.inputType = InputType.TYPE_CLASS_TEXT;

        builder.setTitle(dialogTitle);
        builder.setView(listTitleEditText);

        builder.setPositiveButton(positiveButtonTitle){
                dialog, i ->

            val newList = listTitleEditText.text.toString();
            val newId = UUID.randomUUID().toString();
            //ref.setValue(newlist);
            //ref.child(newId).child("list-name").setValue(newList)
            ref.child(listId).child("list").child(newId).child("list-name-list").setValue(newList);
            dialog.dismiss();
            //showListDetail(newId);
        }

        builder.create().show();
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_ma, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            R.id.action_settings -> true
            else -> super.onOptionsItemSelected(item)
        }
    }




}